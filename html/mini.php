<?php include_once('/var/www/inc/.php');


internal_gate_control();
$uri = $_SERVER['REQUEST_URI'];


# http://mini.i3ns.net/get.php?username=user&password=pass&type=m3u&output=ts
# load the list
if( substr($uri, 0, 9) == '/get.php?' ){

	$rw_s = dbqf(" SELECT `id`, `name` FROM `freestream` ");
	foreach( $rw_s as $rw ){

		extract($rw);

		$url = 'http://'.MINI_PATH.'/user/pass/'.$id;
		$arr[ $url ] = $name;

	}

	header('Content-Disposition: attachment; filename="mini.m3u"');
	header("Content-Type: audio/mpegurl;");
	echo arr_to_m3u( $arr );
	die;


# http://mini.i3ns.net/user/pass/89106
# redirect to source
} else if( substr($uri, 0, 11) == '/user/pass/' AND is_numeric(substr($uri, 11)) ){
	
	$id = substr($uri, 11);
	$rw = dbqf(" SELECT `source`, `relay`, `codec` FROM `freestream` WHERE `id`=$id LIMIT 1 ");
	extract($rw);

	http_response_code(301);
	header("HTTP/1.1 301 Moved Permanently");

	if( $codec ){
		$path = "http://{$codec}.codec.i3ns.net:3913/stream/{$id}/master.m3u8";

	} else if( $relay ){
		$path = "http://{$relay}.relay.i3ns.net:1960/{$source}";

	} else {
		$path = $source;
	}

	header('Location: '.$path, true, 301);
	die;

#
# wrong address
} else {
	echo ':)';
}

