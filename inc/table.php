<?php

# 28 Jul 2020

/*
$rw_deposit = table('xb_deposit', $deposit_id);
$rw_PI = table('xb_paypal_tnx', [
	'PIU_table' => 'deposit',
	'PIU_ID' => $deposit_id,
	'PIU_commit' => 1,
	'cost' => [ '>' , 0 ],
	], $limit=1 );
*/

function table( $table , $id, $limit=null ){

	if( is_array($id) ){
		
		foreach( $id as $k => $v ){
			
			if(! is_array($v) ){
				if( $v === null ){
					$q_where[] = "`$k` is null";					
				} else {
					$q_where[] = "`$k`='$v'";
				}
			
			} else if( strtoupper(trim($v[0])) == "IN" ){
				if( is_array($v[1]) ){
					$v[1] = implode(',', $v[1] );
				}
				$q_where[] = "`$k` IN (".$v[1].")";

			} else {
				$q_where[] = "`$k` ".$v[0]."'".$v[1]."'";
			}
		}

		$q_where = implode(' AND ', $q_where);

		if( $limit ){
			$q_limit = "LIMIT $limit";
		}

	} else {
		$q_where = "`id`=$id";
		$q_limit = "LIMIT 1";
	}

	if(! $rs = mysql_query(" SELECT * FROM `$table` WHERE $q_where $q_limit ") ){
		echo mysql_error();
		return false;

	} else if(! mysql_num_rows($rs) ){
		return false;

	} else if( is_array($id) and $limit!=1 ){
		while( $rw = mysql_fetch_assoc($rs) ){
			$rw_s[] = $rw;
		}
		return $rw_s;

	} else {
		return mysql_fetch_assoc($rs);
	}

}


function dbq( $q ){
	return mysql_query($q);
}



function dbr($rs, $row, $col=null){
	
	if( $col ){
		return mysql_result($rs, $row, $col);
	
	} else {
		return mysql_result($rs, $row);
	}

}


function dbc( $table, $ar=null ){
	$ar_str = iusd_where($ar);
	// echo $ar_str;
	return dbr( dbq(" SELECT COUNT(*) FROM `$table` WHERE 1 $ar_str "), 0, 0);
}


function dbn($rs){
	return mysql_num_rows($rs);
}



function dbf($rs){
	return mysql_fetch_assoc($rs);
}



function dbaf(){
	return mysql_affected_rows();
}



function dbrm( $table, $id ){
	$id = intval($id);
	return dbq(" DELETE FROM `$table` WHERE `id`=$id LIMIT 1 ");
}



function dbe(){
	return mysql_error();
}



function dbi(){
	return mysql_insert_id();
}


function dbqf( $q, $die=false, $key_by_id=false ){ // v-1.2

	$qn = strtolower($q);
	while( strstr($qn, ' ') )
		$qn = str_replace(' ', '', $qn);
	$single_req = ( substr($qn,-6) == 'limit1' );
	
	$rw_s = [];

	if(! $rs = dbq($q) ){
		if( $die ) die;
		echo dbe();

	} else if(! dbn($rs) ){
		if( $die ) die;
		return $single_req 
			? null 
			: [];

	} else while( $rw = dbf($rs) ){
		
		if( $key_by_id ){
			
			if( is_bool($key_by_id) ){
				$key_by_id = 'id';
			
			 } else if( strstr($key_by_id, '>') ){
				list($key_by_id, $the_value) = explode('>', $key_by_id);
			}
			
			$id = $rw[$key_by_id];
			
			if( $the_value ){
				$rw_s[ $id ] = $rw[$the_value];

			} else {
				unset($rw[$key_by_id]);
				$rw_s[ $id ] = $rw;
			}

		} else {
			$rw_s[] = $rw;
		}
	}

	if( $single_req ){
		$rw_s = array_slice($rw_s,0,1);
		if( sizeof($rw_s) )
			$rw_s = $rw_s[0];
	}

	return $rw_s;

}












