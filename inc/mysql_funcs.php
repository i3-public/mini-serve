<?php


function mysql_query( $q, $dp=null ){
	if(! $dp ){
		global $dp;
	}
	return mysqli_query($dp, $q);
}


function mysql_num_rows( $rs ){
	return mysqli_num_rows($rs);
}


function mysql_num_fields( $rs ){
	return mysqli_num_fields($rs);
}


function mysql_result($res, $row, $field=0){ 
    $res->data_seek($row); 
    $datarow = $res->fetch_array(); 
    return $datarow[$field]; 
} 


function mysql_fetch_assoc($res){
	return mysqli_fetch_assoc($res);
}


function mysql_error(){
	global $dp;
	return mysqli_error($dp);
} 


function mysql_affected_rows(){
	global $dp;
	return mysqli_affected_rows($dp);
} 


function mysql_insert_id(){
	global $dp;
	return mysqli_insert_id($dp);
}

function mysql_close(){
	global $dp;
	mysqli_close($dp);
}