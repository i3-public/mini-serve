<?php


function internal_gate_control(){

	// if( $_SERVER['REMOTE_ADDR'] != '95.217.16.140' )
	// 	return true;

	# $url = 'http://tools.i3ns.net/cache/compressed-url/60/'.text_compress('https://xwork.app/api/feed/etc/server_list/?only_ip=1');
	$url = 'https://xwork.app/api/feed/etc/server_list/?only_ip=1';

	$cache_key = md5(__FUNCTION__.$url);

	if(! $ip_s = file_cache($cache_key) ){
		if( $ip_s = fgct($url) ){
			file_cache($cache_key, $ip_s, 600);
		} else {
			die("can't get the list. {$ip_s}");
		}
	}
	
	if(! is_array( $ip_s = json_decode($ip_s, true) ) ){
		die("wrong content of list.");

	} else if(! in_array( $ip = $_SERVER['REMOTE_ADDR'] , $ip_s ) ){
		die('access denied.');
	
	} else {
		return true;
	}

}


function file_cache( $key, $value=null, $time=null ){

	$file = '/tmp/filecache';
	
	if( file_exists($file) ){
		$data = file_get_contents($file);
		$arr = json_decode($data, true);
	} else {
		$arr = [];
	}

	# save
	if( $value !== null ){

		$arr[$key]['value'] = $value;
		$arr[$key]['expire'] = date('U') + ($time ? : 999999999);
		$data = json_encode($arr);

		if(! file_put_contents($file, $data) ){
			echo "Can't save tmp file";
			return false;
		} else {
			return true;
		}
	
	# load
	} else {

		if( array_key_exists($key, $arr) and $arr[$key]['expire'] > date('U') ){
			return $arr[$key]['value'];

		} else {
			return null;
		}
		
	}

}






