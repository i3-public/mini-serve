<?php


function m3u_to_arr( $m3u ){
	
	$m3u = str_replace( ["\r\n"], "\n", $m3u);

	foreach( explode("#EXTINF:-1,", $m3u) as $line ){
		if( strstr($line, 'http://') or strstr($line, 'https://') ){
			list( $name, $url ) = explode("\n", $line);
			$name = trim($name);
			$url = trim($url);

			$arr[ $url ] = $name;
		}
	}

	return $arr;
}


function arr_to_m3u( $arr ){
	// $m3u = "#EXTM3U\n";
	$m3u = '';
	foreach( $arr as $line => $name ){
		$m3u.= "#EXTINF:-1,".$name."\n";
		$m3u.= $line."\n";
	}

	return $m3u;
}


