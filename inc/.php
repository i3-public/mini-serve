<?php

ini_set('display_errors', 'On');

$file = '/var/www/.env';
if(! file_exists($file) ){
    die('no .env found.');
} else {
	$file = file($file);
	if( sizeof($file) ){
		foreach( $file as $line ){
			if(! $line = trim($line,"\r\t\n ") )
				continue;
			if( substr($line, 0, 1) == '#' )
				continue;
			if(! strstr($line, '=') )
				continue;
			$pos = strpos($line, '=');
			$k = substr($line, 0, $pos);
			$v = substr($line, $pos+1 );
			if( strtolower($v) === 'false' ){
				$v = false;
            } else if( strtolower($v) === 'true' ){
				$v = true;
            } else if( strtolower($v) === 'null' ) {
				$v = null;
            }
			define($k, $v);
		}
	}
}

define('MINI_PATH', MINI_HOST.':9027');

foreach( glob('/var/www/inc/*.php') as $file )
	include_once($file);

