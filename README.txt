
#
# main port: 9027
# wget -qO- https://gitlab.com/i3-public/mini-serve/-/raw/main/README.txt | bash

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/docker-installer/docker-installer.sh | bash

wget -O dockerfile https://gitlab.com/i3-public/conf/-/raw/main/nginx-on-docker/conf/dockerfile
sed -i 's,#INJECT,RUN wget -qO- https://gitlab.com/i3-public/mini-serve/-/raw/main/conf/patch | bash,g' dockerfile

docker rm -f mini
docker rmi mini-image

docker build -t mini-image -f dockerfile .
rm -rf dockerfile
mkdir -p /docker/mini && wget -O /docker/mini/.env https://gitlab.com/i3-public/mini-serve/-/raw/main/conf/.env-sample
docker run -t -d --restart unless-stopped --name mini --hostname mini -p 9027:9027 -v /docker/mini/.env:/var/www/.env mini-image
